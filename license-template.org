#+REVEAL_HTML: <div class="slide-footer"><br></div></section><section id="slide-license" data-state="no-toc-progress"><h3 class="no-toc-progress">License Information</h3>

Except where otherwise noted, this work, “{{{title}}}”,
is © 2018, under the Creative Commons license
[[https://creativecommons.org/licenses/by-sa/4.0/][CC BY-SA 4.0]].


[[mailto:tux.eithel@gmail.com][Cristian Pavan]]

[[mailto:stefano@uomorando.it][Stefano Morandi]]
